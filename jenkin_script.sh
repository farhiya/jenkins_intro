echo "Please enter your ip address: "

read ip_address

COUNTER=0
for arg in $ip_address
do
COUNTER=$(expr $COUNTER + 1)
done

if ((COUNTER == 1))
then 
    echo "This is your ip $arg" 

elif ((COUNTER < 1))
then
    echo "There was no input. Try again! "
    
    # run's the file again. This will loop until 1 input is entered
    # ./redhat_bash_script.sh

    # `exit 1` will stop bash script
    exit 1
elif ((COUNTER > 1))
then

    echo "That was too many inputs. Try again! "
    #To run script again ->  ./redhat_bash_script.sh
    exit 1

fi

# my jenkins ip: 52.31.111.103
# opening ec2 linux machine and removing fingerprint check at given ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/ch9_shared.pem ubuntu@$ip_address '

# update all packages in machine
sudo apt update

# install java packages
sudo apt install default-jre -y
sudo apt install default-jdk -y

# add repo key to system
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -

# append the Debian package repository address to the server’s sources.list
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'

# update all packages in machine
sudo apt update

# install jenkins
sudo apt install jenkins -y

# start jenkins
sudo systemctl start jenkins

# show jenkins status
sudo systemctl status jenkins

# In the terminal window, use the cat command to display the password
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

# jenkins admin password - 70762c0d21764eb5a3741b6c944c02c6
'
